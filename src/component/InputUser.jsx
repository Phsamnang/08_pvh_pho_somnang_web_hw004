import React, { Component } from 'react';
import TableComponent from './TableComponent';

class InputUser extends Component {
    constructor() {
        super();
        this.state = {
            email: "null",
            username: "null",
            age: "null",
            status: false,
            userArr: [
            ],
        };
        // this.handlerChangeIsActive=this.handlerChangeIsActive.bind(this);
    }
    handlerChangeIsActive = (index) => {
        const user = this.state.userArr[index];
        user.status = !user.status
        this.setState({
            userArr: [...this.state.userArr]
        })
    }

    handleChangeValue = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        },)
        //

    }
    onSubmit = () => {
        //console.log(this.state.userArr);
        const newObj = { id: this.state.userArr.length + 1, email: this.state.email, username: this.state.username, age: this.state.age, status: false };
        this.setState({
            userArr: [...this.state.userArr, newObj]
        }, () => console.log(this.state.userArr))
    }
    render() {
        return (
            <div className='m-10'>
                <h1 className='text-4xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-blue-500 to-purple-500 text-center'>Registeration Information1</h1>
                <div className='m-10'>
                    <label for="input-group-1" class="block mb-2 text-2xl font-medium text-black">Your Email</label>
                    <div class="relative mb-6">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 " fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                        </div>
                        <input type="text" id="input-group-1" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 " placeholder="example@gmail.com" onChange={this.handleChangeValue} name="email" />
                    </div>
                    <label for="website-admin" class="block mb-2 text-2xl font-medium text-black">Username</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md">
                            @
                        </span>
                        <input type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  " placeholder="Username" onChange={this.handleChangeValue} name="username" />
                    </div>
                    <label for="website-admin" class="block mb-2 text-2xl font-medium text-black">Age</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md">
                            👉🏼
                        </span>
                        <input type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  " placeholder="Age" onChange={this.handleChangeValue} name="age" />
                    </div>
                    <div className='flex justify-center mt-10'>
                        <button type="button" class="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 " onClick={this.onSubmit}>Save</button>

                    </div>
                    <TableComponent data={this.state.userArr} handler={this.handlerChangeIsActive} className="w-full" />
                </div>


            </div>

        );
    }
}

export default InputUser;