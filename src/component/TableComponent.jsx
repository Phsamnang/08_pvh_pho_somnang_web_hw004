import React, { Component } from 'react';
import Swal from 'sweetalert2';
class TableComponent extends Component {
    showInfo= (data) => {
        Swal.fire({
            title: "ID:"+`${data.id}`+"</br>"+"Email:"+`${data.email}`+"</br>"+"Username:"+`${data.username}`+"</br>"+"Age:"+`${data.age}`,
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
          })
    }
    
    render() {
        return (
                <div >
                    <table class="table-auto border-collapse border border-slate-300 w-full text-center">
                    <thead>
                        <tr class="bg-indigo-400 text-white">
                            <td class="p-2">ID</td>
                            <td class="p-2">Email</td>
                            <td class="p-2">UserName</td>
                            <td class="p-2">Age</td>
                            <td class="p-2">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item,index) => (
                            <tr class="even:bg-red-300 odd:bg-blue-100">
                                <td class="p-2">{item.id}</td>
                                <td class="p-2">{item.email}</td>
                                <td class="p-2">{item.username}</td>
                                <td class="p-2">{item.age}</td>
                                <td class="p-2"><button className={item.status?'bg-green-500  text-white font-bold py-2 px-4 rounded':'bg-red-500  text-white font-bold py-2 px-4 rounded'} onClick={()=>{this.props.handler(index)}}>{item.status?'Done':'Pending'}</button>
                                <button className='bg-blue-500  text-white font-bold py-2 px-4 rounded m-4'onClick={()=>{this.showInfo(item)}}>Show more</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TableComponent